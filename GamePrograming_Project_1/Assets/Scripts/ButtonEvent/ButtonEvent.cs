﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace ButtonEvent

{
    public class ButtonEvent : MonoBehaviour
    {

        public void NextLevel()
        {
            SceneManager.LoadScene("Game3");
        }

        public void Back()
        {
            SceneManager.LoadScene("Game");
        }

        public void Quit()
        {
            Application.Quit();
            Debug.Log("Quit Game");
        }
    }
}
